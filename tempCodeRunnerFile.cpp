#include <iostream>
#include <cmath>

using namespace std;

int main() {
    // Frage nach der Dezimalzahl
    cout << "Bitte geben Sie eine Dezimalzahl ein: ";
    int decimal;
    cin >> decimal;

    // Konvertiere die Dezimalzahl in eine Binärzahl
    string binary = "";
    while (decimal > 0) {
        binary = to_string(decimal % 2) + binary;
        decimal /= 2;
    }

    // Gib die Binärzahl aus
    if (binary == "") {
        binary = "0";
    }
    cout << "Die Binärzahl von " << decimal << " ist " << binary << endl;

    return 0;
}
