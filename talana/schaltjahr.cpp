#include <iostream>
using namespace std;

int main() {

  int jahr;
  bool quit = false;

  do {
    cout << "Schaltjahr oder nicht? ";
    cin >> jahr;

    if (jahr % 400 == 0 && jahr % 100 == 0) {
      cout << jahr << " ist ein Schaltjahr" << endl;
    } else {
      cout << jahr << " ist kein Schaltjahr" << endl;
    }

    cout << "Do you want to check another year? (Y/N) ";

    char choice;
    cin >> choice;

    if (choice == 'N' || choice == 'n') {
      quit = true;
    }
  } while (!quit);

  std::cout << "okay" << std::endl;

  return 0;
}
